from flask import Flask, render_template, abort, make_response, request
import os
import logging

app = Flask(__name__)


@app.route('/<path:subpath>')
def get_file(subpath):
    file_name = subpath
    source_path = "./templates/" + file_name
    if subpath.find("//") != -1 or subpath.find("~") != -1 or subpath.find("..") != -1:
        abort(403)
    elif os.path.exists(source_path):
        return render_template(file_name), 200
    else:
        abort(404)


@app.errorhandler(403)
def error_403(e):
    return render_template('403.html'), 403


@app.errorhandler(404)
def error_404(e):
    return render_template('404.html'), 404


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
